* * * * * if [ `ps aux | grep "[o]ms:catalog-export:run" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento oms:catalog-export:run; fi
1 17 * * * php ~/app/current/bin/magento central:category-product-count:category
30 7 * * * php ~/app/current/bin/magento tops:product-status:consume
30 8 * * * php ~/app/current/bin/magento tops:product-status:disable
30 9 * * * php ~/app/current/bin/magento tops:product-status:enable
* * * * * if [ `ps aux | grep "[t]ops:order-integration:send --system=report" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento tops:order-integration:send --system=report; fi
* * * * * if [ `ps aux | grep "[t]ops:order-integration:send --system=pickingtool" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento tops:order-integration:send --system=pickingtool; fi
*/5 * * * * if [ `ps aux | grep "[p]ricing:sync" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento pricing:sync
*/15 * * * * if [ `ps aux | grep "[p]ricing:update" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento pricing:update; fi
* * * * * sleep 15 && MAGE_INDEXER_THREADS_COUNT=1 php ~/app/current/bin/magento central:mview:update --mview_id=tops_promotion_product_discount
0 */3 * * * sleep 15 && MAGE_INDEXER_THREADS_COUNT=1 php ~/app/current/bin/magento central:mview:update --mview_id=central_category_product_count_by_category
* * * * * if [ `ps aux | grep "[e]lasticsuite:reindex" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then MAGE_INDEXER_THREADS_COUNT=7 php ~/app/current/bin/magento elasticsuite:reindex --fulltext; fi
* * * * * if [ `ps aux | grep "[m]com:payment:validate-notify" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento mcom:payment:validate-notify; fi
* * * * * if [ `ps aux | grep "[t]ops:order:send-notification" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento tops:order:send-notification; fi
* * * * * if [ `ps aux | grep "[c]entral:product-updater-queue:process" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento central:product-updater-queue:process --limit=2000; fi
* * * * * if [ `ps aux | grep "[s]ync_orders" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then cd ~/app/current/ &&  CRON_FORCE_MODE=true ~/app/current/n98-magerun2.phar sys:cron:run sync_orders; fi
* * * * * if [ `ps aux | grep "[c]atalog_product_price" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then sleep 15 && MAGE_INDEXER_THREADS_COUNT=1 php ~/app/current/bin/magento tops:custom-sql:reset-price-mview && php ~/app/current/bin/magento central:mview:update --mview_id=catalog_product_price; fi
* * * * * if [ `ps aux | grep "[c]ataloginventory_stock" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then sleep 15 && MAGE_INDEXER_THREADS_COUNT=1 php ~/app/current/bin/magento central:mview:update --mview_id=cataloginventory_stock; fi
*/30 * * * * php ~/app/current/bin/magento tops:custom-sql:remove-duplicate-index
1 17 * * * php ~/app/current/bin/magento tops:custom-sql:promotions-index
1 5 * * * php ~/app/current/bin/magento tops:custom-sql:update-stock-item-status
*/5 * * * * if [ `ps aux | grep "[t]ops_new_customer_segment_update_condition_date" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then cd ~/app/current/ && CRON_FORCE_MODE=true ~/app/current/n98-magerun2.phar sys:cron:run tops_new_customer_segment_update_condition_date; fi
00 01 29 * * php ~/app/current/bin/magento tops:sales-sequence:prepare-order-sequence
00 01 05 * * php ~/app/current/bin/magento tops:sales-sequence:remove-inactive
00 16 * * * php ~/app/current/bin/magento tops:customer:update-language
* * * * * if [ `ps aux | grep "[o]rder:salesrule:process" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then /usr/bin/php ~/app/current/bin/magento order:salesrule:process; fi
0 */5 * * * php ~/app/current/bin/magento indexer:reset catalog_category_flat && php ~/app/current/bin/magento indexer:reindex catalog_category_flat
* * * * * if [ `ps aux | grep "[t]ops:order-integration:snapshot" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento tops:order-integration:snapshot ; fi
* * * * * if [ `ps aux | grep "[s]ales_send_order_emails" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then cd ~/app/current/ && CRON_FORCE_MODE=true ~/app/current/n98-magerun2.phar sys:cron:run sales_send_order_emails; fi
* * * * * if [ `ps aux | grep "[c]entral:order-fms-integration:snapshot" | grep -v "/bin/sh" | wc -l` -lt 1 ]; then php ~/app/current/bin/magento central:order-fms-integration:snapshot ; fi
0 13 * * * php ~/app/current/bin/magento indexer:reindex cataloginventory_stock
*/30 * * * * php ~/app/current/bin/magento mcom:status-history:add-authorized
0 20 * * * php ~/app/current/bin/magento central:cron:clean