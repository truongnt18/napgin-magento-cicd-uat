# Tops E-Commerce - Magento 2.3 #

# Releases 

3.3.0
============

Update custom-cron.txt

[TOL-6564](https://cenergy.atlassian.net/browse/TOL-6564) - Error /Webapi/Rest/Central\ServiceBus\Api\MessageProcessorInterface/process.

[TOL-6570](https://cenergy.atlassian.net/browse/TOL-6570) - No payment failed email send after unsuccessful checkout with 2c2p.

[TOL-5824](https://cenergy.atlassian.net/browse/TOL-5824) - Run particular indexers using custom cron job schedule.

[TOL-6369](https://cenergy.atlassian.net/browse/TOL-6369) - Invalid created order date on email.

[TOL-6553](https://cenergy.atlassian.net/browse/TOL-6553) - add unit test for tops-product-bundle-fix-special-price.

####Repositories Updated

[tops/product-bundle-fix-special-price](https://bitbucket.org/centraltechnology/tops-product-bundle-fix-special-price/commits/547073d4e50d77d07f796f5a11156c5202466c86) : 2.0.5 to 2.0.6

[central/quote-plugin-fix](https://bitbucket.org/centraltechnology/central-quote-plugin-fix/commits/4c5703a7551bcee04711ae8bfd828f3c52a01564) : 1.0.1 to 2.0.0

[tops/emarsys](https://bitbucket.org/centraltechnology/tops-emarsys/commits/cbd839d3d232f6ebdebf1ae0e426026d0fd5b63c) : 1.1.2 to 1.1.3

[central/indexer](https://bitbucket.org/centraltechnology/central-indexer/commits/bd0d2a3b739b0df980bc014f2300bbc2163d5ffb) : 1.1.1 to 1.1.2

[tops/emarsys](https://bitbucket.org/centraltechnology/tops-emarsys/commits/37b2e08a6cdb378cce9a644ed8506d2bf5ba9f74) : 1.1.1 to 1.1.2

[tops/product-bundle-fix-special-price](https://bitbucket.org/centraltechnology/tops-product-bundle-fix-special-price/commits/f09003fd1fe360fdae6e4822b2317b1b56517ec4#chg-composer.json) : 2.0.4 : 2.0.5

3.2.0
============
[TOL-6506](https://cenergy.atlassian.net/browse/TOL-6506) - Investigate error 'No Invoice created for order xxx'

[TOL-6167](https://cenergy.atlassian.net/browse/TOL-6167) - Improve cron indexer_clean_all_changelogs to avoid database lock.

[TOL-5838](https://cenergy.atlassian.net/browse/TOL-5838) - Emarsys log improvement.

[TOL-5941](https://cenergy.atlassian.net/browse/TOL-5941) - Fixed Emarsys log cleaner.

[TOL-5620](https://cenergy.atlassian.net/browse/TOL-5620) - Store pickup missing district , sub district after set status on grid.

[TOL-5824](https://cenergy.atlassian.net/browse/TOL-5824) - Run particular indexers using custom cron job schedule.

[TOL-6453](https://cenergy.atlassian.net/browse/TOL-6453) - Handle CategoryProductCount::getProductCount().

[TOL-6321](https://cenergy.atlassian.net/browse/TOL-6321) - Payment method for Hungry Ghost.

[TOL-6404](https://cenergy.atlassian.net/browse/TOL-6404) - Return shipping method description depends on item on cart.

[TOL-6322](https://cenergy.atlassian.net/browse/TOL-6322) - Shipping method and slot for Hungry Ghost.

[TOL-6510](https://cenergy.atlassian.net/browse/TOL-6510) - Fix calculates max qty can apply for CPN2.

[TOL-6516](https://cenergy.atlassian.net/browse/TOL-6516) - FMS order integration: Validate payment data before snap order.

####Repositories Added

[central/clean-changelogs](https://bitbucket.org/centraltechnology/central-clean-changelogs/commits/542c03b8ceef4ebdaba7331171cd56d3b46b6cda) : 1.0.0

[central/emarsys-spilt-db](https://bitbucket.org/centraltechnology/central-emarsys-spilt-db/commits/0de999add43cb0655bb31294ab1947a453e815a8) : 1.0.0

[tops/seasonal](https://bitbucket.org/centraltechnology/tops-seasonal/commits/eebb6f3f374855a765e63a121db6451972b50707) : 1.1.1

####Repositories Updated

[tops/emarsys](https://bitbucket.org/centraltechnology/tops-emarsys/commits/a868bedea29bbf05cd79adebf0b47763f5a6ffcf) : 1.1.0 to 1.1.1

[tops/bundle-sale-api](https://bitbucket.org/centraltechnology/tops-bundle-sale-api/commits/a684be26334c00cd0f44a1a98f411e567fd607a8) : 1.1.1 to 1.2.0

[central/store-locator](https://bitbucket.org/centraltechnology/central-store-locator/commits/1e41d6760e8638ce986d960947e0d97f61373ef3) : 0.1.5 to 0.1.10

[central/category-product-count-indexer](https://bitbucket.org/centraltechnology/central-category-product-count-indexer/commits/11a8be72bc7c03d034a5d2501e78e212d84ad124) : 1.0.6 to 1.0.9

[central/shipping-slot](https://bitbucket.org/centraltechnology/central-shipping-slot/commits/b8550d657eeca90d8aaea194fb11059c454279e7) : 2.0.24 to 2.0.27

[central/fms-integration](https://bitbucket.org/centraltechnology/central-fms-integration/commits/602a8d3e27f34b7fe4d0d7b966d014909350bbb1) : 1.2.3 to 1.2.4

[tops/fms-integration](https://bitbucket.org/centraltechnology/tops-fms-integration/commits/de1e376c5ba70400a5265e0188288e0983c652b2) : 1.0.8 to 1.1.0

[central/shipping-slot-change](https://bitbucket.org/centraltechnology/central-shipping-slot-date/commits/67c4c960b79f7a370aee25ec1e546e664822110b) : 1.0.5 to 1.0.6

###Patches

- update-model-product-getweight.patch

3.1.0
============
[TOL-6368](https://cenergy.atlassian.net/browse/TOL-6368) - Improve order integration APIs about address line.

[TOL-6332](https://cenergy.atlassian.net/browse/TOL-6332) - Add to cart speed improvement for pack item.

[TOL-6238](https://cenergy.atlassian.net/browse/TOL-6238) - Provide promotion start date.

[TOL-6372](https://cenergy.atlassian.net/browse/TOL-6372) - FMS order integration grid view.

[TOL-6362](https://cenergy.atlassian.net/browse/TOL-6362) - Coupon cannot be apply with SKU Bundle Item.

[TOL-6302](https://cenergy.atlassian.net/browse/TOL-6302) - Return the least category_id and name of level 4 from MDC to "category_en" field at gtm_data section.

[TOL-6375](https://cenergy.atlassian.net/browse/TOL-6375) - Validate district and subdistrict in Shipping and Billing address.

[TOL-6377](https://cenergy.atlassian.net/browse/TOL-6377) - TOPSSD-3089 - Billing Address show as Shipping address in WMS (Picking Tool).

[TOL-6162](https://cenergy.atlassian.net/browse/TOL-6162) - Add optimized mappings for search to automated indice creation in Tops.

[TOL-6383](https://cenergy.atlassian.net/browse/TOL-6383) - Order CW031200700622 Error was: Incorrect (CustomerPayAmt + t1c redeem amt).

[TOL-6417](https://cenergy.atlassian.net/browse/TOL-6417) - Set default value to support FMS cases.

[TOL-6424](https://cenergy.atlassian.net/browse/TOL-6424) - Order integration to FMS Validate T1C and Tax ID.

[TOL-6388](https://cenergy.atlassian.net/browse/TOL-6388) - Order: FC432200708930 - No Credit Card info to FMS.

[DS-649](https://cenergy.atlassian.net/browse/DS-649) - Config TOPS MDC/ES to support A/B Testing.

####Repositories Update

[tops/data-integration](https://bitbucket.org/centraltechnology/tops-data-integration/commits/6238b4cda1d5e06ba9dd2269cc508b371a192b33) : 1.1.13 to 1.1.15

[central/fms-integration](https://bitbucket.org/centraltechnology/central-fms-integration/commits/13e7fef95306647ae016ad220530581d70e53d66) : 1.1.24 to 1.2.3

[tops/bundle-stock](https://bitbucket.org/centraltechnology/tops-bundle-stock/commits/566ca4119ddba2df546ec4940963742e64762fa4) : 1.1.3 to 1.1.4

[tops/salesrule-generator](https://bitbucket.org/centraltechnology/tops-salesrule-generator/commits/a461c5331301cd3a8266222f197e60565e241de6) : 1.6.1 to 1.6.2

[tops/elasticsuite-promotion-indexer](https://bitbucket.org/centraltechnology/tops-elasticsuite-promotion-indexer/commits/c9a981a20117c94914a806241ab0cc0532912fc2) : 1.0.3 to 1.0.6

[tops/sales-order-attributes](https://bitbucket.org/centraltechnology/tops-sales-order-attributes/commits/34f13c54358a36d6c297deaab27d4990715113ae) : 1.1.4 to 1.1.5

[tops/gtm](https://bitbucket.org/centraltechnology/tops-gtm/commits/0a362a242db22357fe8c906fbf146506e3a779c0) : 2.0.7 to 2.0.8

[tops/fms-integration](https://bitbucket.org/centraltechnology/tops-fms-integration/commits/12d11dd769b69a3bee150ec104356097ef8bab90) : 1.0.7 to 1.0.8

[central/elasticsuite-indexer-custom-index-analysis](https://bitbucket.org/centraltechnology/central-elasticsuite-indexer-custom-index-analysis/commits/dfe14910ebeea25666ca5eccc6f6bf60b8fd1df3) : 1.0.2 to 1.1.2

####Repositories Added

[tops/salesrule-product-subselect-bundle](https://bitbucket.org/centraltechnology/tops-salesrule-product-subselect-bundle/commits/696149805f15679386d9b2e0daeec19a2268a637) : 1.0.0

[central/elasticsuite-optimized-mappings](https://bitbucket.org/centraltechnology/central-elasticsuite-optimized-mappings/commits/) : 1.0.1


###Patches

- set-default-value-to-tops-fms-integration.patch

3.0.0
============
[TOL-2689](https://cenergy.atlassian.net/browse/TOL-2689) - Bypass POS by integrating with FMS.

[TOL-6057](https://cenergy.atlassian.net/browse/TOL-6057) - Apply stock speed improvement.

[TOL-6293](https://cenergy.atlassian.net/browse/TOL-6293) - Disable SalesRule fields name and description on edit mode


####Repositories Added

[central/advanced-sales-rule-category](https://bitbucket.org/centraltechnology/central-advanced-sales-rule-category/commits/757d8ee3a96060dc360bfcecbcfbaf0d72ac91cb) : 1.0.0

[central/fms-integration](https://bitbucket.org/centraltechnology/central-fms-integration/commits/69f99d3eb4ebc638d54faae3c43758251cad3ee4) : 1.1.24

[central/indexer](https://bitbucket.org/centraltechnology/central-indexer/commits/c111b3e3c0f43741a6b33456771ee3a2c20bf135) : 1.1.1

[central/limit-order-qty](https://bitbucket.org/centraltechnology/central-limit-order-qty/commits/16655367d4ab86b28026b28a67fa2f7df0b5809a) : 1.0.1

[central/payment-restriction-customer-group](https://bitbucket.org/centraltechnology/central-payment-restriction-customer-group/commits/022588963363eb4842e8a0ee0b9aaa8523b09e54) : 1.0.0

[tops/bundle-stock](https://bitbucket.org/centraltechnology/tops-bundle-stock/commits/23ba9f6767b9ac529ec885c01af0e82c122ab467) : 1.1.3

[tops/credit-term](https://bitbucket.org/centraltechnology/tops-credit-term/commits/d894aa2091f344681883985856ac308c9bc6a6cb) : 1.0.0

[tops/fms-integration](https://bitbucket.org/centraltechnology/tops-fms-integration/commits/94d81b13662b03049754d4d54d0d8bc2adcd0c6e) : 1.0.7

[tops/payment-mcom-credit-term](https://bitbucket.org/centraltechnology/tops-payment-mcom-credit-term/commits/7289f444c4c10d0110222851bad389a4933e1da9) : 1.0.0

[tops/product-bundle-fix-special-price](https://bitbucket.org/centraltechnology/tops-product-bundle-fix-special-price/commits/226e69ce0c1475353290f3bb229d360b7744f7bd) : 2.0.4

[tops/sales-adjust](https://bitbucket.org/centraltechnology/tops-sales-adjust/commits/c602ae1a187f43ddc088be348ca132c9af811418) : 1.0.3

[central/mcom-stock-update-optimization](https://bitbucket.org/centraltechnology/central-mcom-stock-update-optimization/commits/c645eaf9d5209dae385c34a1abbefdc229e36419) : 1.0.1

####Repositories Update

apply patch Skip catalog category product index

[tops/bundle-sale-api](https://bitbucket.org/centraltechnology/tops-bundle-sale-api/commits/41de3f5847bce436be4a04199eda596a0ea5a91a) : 1.0.0 to 1.1.1

[tops/emarsys](https://bitbucket.org/centraltechnology/tops-emarsys/commits/70f815df704be0bbdcb01584e76fca45fa20cd82) : 1.0.19 to 1.1.0

[tops/product-groups](https://bitbucket.org/centraltechnology/tops-product-groups/commits/2f3321af7e87be4578ac4f93e45fe1942034e321) : 1.0.6 to 1.0.7

[tops/salesrule-generator](https://bitbucket.org/centraltechnology/tops-salesrule-generator/commits/87757427cd75f3374ff8f79a37fa26e555253463) : 1.2.1 to 1.6.1

[central/urlrewrite](https://bitbucket.org/centraltechnology/central-urlrewrite/commits/9dea8a86e10215d9d9e214f4b3d2e9e426234f78) : dev-no-brand to 0.2.0

[tops/salesrule-additional](https://bitbucket.org/centraltechnology/tops-salesrule-additional/commits/231b330095d55c67506806803ec86f344b9cf0e7) : 1.1.3 to 1.1.4

2.0.1
============
[TOL-6151](https://cenergy.atlassian.net/browse/TOL-6151) - TOPSSD-2907_Credit card orders status stuck ONHOLD on MCOM case payment failed.

[TOL-6132](https://cenergy.atlassian.net/browse/TOL-6132) - TOPSSD-2907_Credit card orders status stuck ONHOLD on MCOM case payment success.

[TOL-6166](https://cenergy.atlassian.net/browse/TOL-6166) - Modify customer segment command.

[TOL-6148](https://cenergy.atlassian.net/browse/TOL-6148) - Create command to update stock status to OOS when qty = 0 for simple products.

[TOL-6140](https://cenergy.atlassian.net/browse/TOL-6140) - Resync order integration.

####Repositories Update

[central/payment-mcom](https://bitbucket.org/centraltechnology/central-payment-mcom/commits/6cad14a723ff131e45e4261afd4daa37fd67b338) : 1.1.3 to 1.1.5

[tops/custom-sql](https://bitbucket.org/centraltechnology/tops-custom-sql/commits/fe9f59dcd91820b444a802ef5aeefd2553bf8365) : 1.0.0 to 1.0.1

[tops/data-integration](https://bitbucket.org/centraltechnology/tops-data-integration/commits/1478132ea485e7b0fdfa21dcfeaecae28a915946) : 1.1.7 to 1.1.13

2.0.0
============
[TOL-5929](https://cenergy.atlassian.net/browse/TOL-5929) - Click product on PLP page then go to home page

[TOL-5867](https://cenergy.atlassian.net/browse/TOL-5867) - [MONITOR] TOPSSD-2723_Product didn't show on website

[TOL-5934](https://cenergy.atlassian.net/browse/TOL-5934) - Sign In with Apple

[TOL-5941](https://cenergy.atlassian.net/browse/TOL-5941) - Fixed Emarsys CleanLog cron job.

####Repositories Update

[central/elasticsearch-mview-price-indexer](https://bitbucket.org/centraltechnology/central-elasticsearch-mview-price-indexer/commits/7839ecaaac2933faa59ed59f9edf3975b46d8194) : 1.0.4 to 1.0.5

[central/elasticsearch-mview-stock-indexer](https://bitbucket.org/centraltechnology/central-elasticsearch-mview-stock-indexer/commits/a0c66d9caf3abeac7357c8df4faacb95e578b2be) : 1.0.3 to 1.1.4

[central/social-auth](https://bitbucket.org/centraltechnology/central-social-auth/commits/d6fc584660a305bdb2111b8467a82bc907b676f9) : 2.0.2 to 2.1.0 

[tops/product-url-rewrite](https://bitbucket.org/centraltechnology/tops-product-url-rewrite/commits/db88e037cbd8a1aa70574c32e1ffaf68de618152) : 1.0.0

[Emarsys\Emarsys](https://bitbucket.org/centraltechnology/tops-mdc23/commits/215ad26f6af1bf573fd073fca9cccad2d771db3d)
