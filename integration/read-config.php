<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
require __DIR__ . '/../app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$reader = $obj->get(\Magento\Framework\App\DeploymentConfig::class);

$allModules = $obj->get(\Magento\Framework\Module\FullModuleList::class);
$enabledModuleList = $obj->get(\Magento\Framework\Module\ModuleList::class);

$all = $allModules->getNames();
$enabled = $enabledModuleList->getNames();
$disabledModules = array_diff($all, $enabled);
$finalDisabled = "'" . implode("', '", $disabledModules) ."'";
$configurations = "<?php
    return [
    'db-host' => 'dbintegration',
    'db-user' => 'root',
    'db-password' => 'magento',
    'db-name' => 'magento_integration_test',
    'db-prefix' => '',
    'backend-frontname' => 'backend',
    'search-engine' => 'elasticsearch7',
    'elasticsearch-host' => 'elasticsearch',
    'elasticsearch-port' => '9200',
    'elasticsearch-enable-auth' => false,
    'elasticsearch-index-prefix' => 'magento2',
    'elasticsearch-timeout' => 15,
    'amqp-host' => 'rabbitmq',
    'amqp-port' => '5672',
    'amqp-user' => 'guest',
    'amqp-password' => 'guest',
    'admin-user' => \Magento\TestFramework\Bootstrap::ADMIN_NAME,
    'admin-password' => \Magento\TestFramework\Bootstrap::ADMIN_PASSWORD,
    'admin-email' => \Magento\TestFramework\Bootstrap::ADMIN_EMAIL,
    'admin-firstname' => \Magento\TestFramework\Bootstrap::ADMIN_FIRSTNAME,
    'admin-lastname' => \Magento\TestFramework\Bootstrap::ADMIN_LASTNAME,
    'disable-modules'   => join(
        ',',[
        $finalDisabled
        ])
];";

$filename = __DIR__.'/install-config-mysql.php';
if(file_put_contents($filename, $configurations)) {
    echo "File install-config-mysql.php was created";
    echo "\n";
}