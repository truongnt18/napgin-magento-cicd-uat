<?php
/**
 * @category   Emarsys
 * @package    Emarsys_Emarsys
 * @copyright  Copyright (c) 2017 Emarsys. (http://www.emarsys.net/)
 */
namespace Emarsys\Emarsys\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\TransportInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail;
use Magento\Framework\Mail\EmailMessageInterface;

/**
 * Class Transport
 * @package Emarsys\Emarsys\Model
 */
class Transport extends \Zend_Mail_Transport_Sendmail implements TransportInterface
{
    /**
     * @var EmailMessageInterface|\Zend_Mail
     */
    protected $_message;

    /**
     * @var SendEmail
     */
    protected $sendEmail;
    /**
     * @var Sendmail
     */
    private $zendTransport;

    /**
     * Transport constructor.
     * @param EmailMessageInterface $message
     * @param SendEmail $sendEmail
     * @param null $parameters
     */
    public function __construct(
        EmailMessageInterface $message,
        SendEmail $sendEmail,
        $parameters = null
    ) {
        if (!$message instanceof \Zend_Mail) {
            if (!$message instanceof EmailMessageInterface) {
                throw new \InvalidArgumentException('Invalid message instance');
            }
        }

        parent::__construct($parameters);
        $this->_message = $message;
        $this->sendEmail = $sendEmail;
        $this->zendTransport = new Sendmail($parameters);
    }

    /**
     * @throws MailException
     */
    public function sendMessage()
    {
        try {
            $emailErrorSendingStatus = $this->sendEmail->sendMail($this->_message);
            if ($emailErrorSendingStatus) {
                if ($this->_message instanceof \Zend_Mail) {
                    parent::send($this->_message);
                }
                if ($this->_message instanceof EmailMessageInterface) {
                    ObjectManager::getInstance()->get(Sendmail::class)->send(
                        Message::fromString($this->_message->getRawMessage())
                    );
                }
            }
        } catch (\Exception $e) {
            throw new MailException(__($e->getMessage()), $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function getMessage()
    {
        return $this->_message;
    }
}
