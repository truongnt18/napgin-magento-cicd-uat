<?php
/**
 * @category   Emarsys
 * @package    Emarsys_Emarsys
 * @copyright  Copyright (c) 2017 Emarsys. (http://www.emarsys.net/)
 */
namespace Emarsys\Emarsys\Model\Template;

use Magento\Framework\App\TemplateTypesInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\AddressConverter;
use Magento\Framework\Mail\EmailMessageInterfaceFactory;
use Magento\Framework\Mail\Exception\InvalidArgumentException;
use Magento\Framework\Mail\MessageInterface;
use Magento\Framework\Mail\MessageInterfaceFactory;
use Magento\Framework\Mail\MimeInterface;
use Magento\Framework\Mail\MimeMessageInterfaceFactory;
use Magento\Framework\Mail\MimePartInterfaceFactory;
use Magento\Framework\Mail\Template\FactoryInterface;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Mail\TransportInterfaceFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class TransportBuilder
 * @package Emarsys\Emarsys\Model\Template
 */
class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    /**
     * @var string
     */
    public $productCollObj = '';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    private $messageData = [];

    /** @var MimeMessageInterfaceFactory|null  */
    private $mimeMessageInterfaceFactory;

    /**
     * @var MimePartInterfaceFactory|null
     */
    private $mimePartInterfaceFactory;

    /** @var EmailMessageInterfaceFactory|null  */
    private $emailMessageInterfaceFactory;

    /** @var AddressConverter|null  */
    private $addressConverter;

    /**
     * TransportBuilder constructor.
     * @param StoreManagerInterface $storeManager
     * @param FactoryInterface $templateFactory
     * @param MessageInterface $message
     * @param SenderResolverInterface $senderResolver
     * @param ObjectManagerInterface $objectManager
     * @param TransportInterfaceFactory $mailTransportFactory
     * @param MessageInterfaceFactory|null $messageFactory
     * @param EmailMessageInterfaceFactory|null $emailMessageInterfaceFactory
     * @param MimeMessageInterfaceFactory|null $mimeMessageInterfaceFactory
     * @param MimePartInterfaceFactory|null $mimePartInterfaceFactory
     * @param AddressConverter|null $addressConverter
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        FactoryInterface $templateFactory,
        MessageInterface $message,
        SenderResolverInterface $senderResolver,
        ObjectManagerInterface $objectManager,
        TransportInterfaceFactory $mailTransportFactory,
        MessageInterfaceFactory $messageFactory = null,
        EmailMessageInterfaceFactory $emailMessageInterfaceFactory = null,
        MimeMessageInterfaceFactory $mimeMessageInterfaceFactory = null,
        MimePartInterfaceFactory $mimePartInterfaceFactory = null,
        AddressConverter $addressConverter = null
    ) {
        $this->storeManager = $storeManager;
        $this->addressConverter = $addressConverter;
        $this->emailMessageInterfaceFactory = $emailMessageInterfaceFactory ?: $objectManager
            ->get(EmailMessageInterfaceFactory::class);
        $this->mimeMessageInterfaceFactory = $mimeMessageInterfaceFactory ?: $objectManager
            ->get(MimeMessageInterfaceFactory::class);
        $this->mimePartInterfaceFactory = $mimePartInterfaceFactory ?: $objectManager
            ->get(MimePartInterfaceFactory::class);
        $this->addressConverter = $addressConverter ?: $objectManager
            ->get(AddressConverter::class);
        parent::__construct(
            $templateFactory,
            $message,
            $senderResolver,
            $objectManager,
            $mailTransportFactory,
            $messageFactory,
            $emailMessageInterfaceFactory,
            $mimeMessageInterfaceFactory,
            $mimePartInterfaceFactory,
            $addressConverter
        );
    }

    /**
     * Get mail transport
     *
     * @return $this
     * @throws LocalizedException
     */
    public function prepareMessage()
    {
        $handle = '';
        $template = $this->getTemplate();
        $body = $template->processTemplate();
        switch ($template->getType()) {
            case TemplateTypesInterface::TYPE_TEXT:
                $part['type'] = MimeInterface::TYPE_TEXT;
                break;

            case TemplateTypesInterface::TYPE_HTML:
                $part['type'] = MimeInterface::TYPE_HTML;
                break;

            default:
                throw new LocalizedException(
                    new Phrase('Unknown template type')
                );
        }
        $mimePart = $this->mimePartInterfaceFactory->create(['content' => $body]);
        $this->messageData['body'] = $this->mimeMessageInterfaceFactory->create(
            ['parts' => [$mimePart]]
        );

        $this->messageData['subject'] = html_entity_decode(
            (string)$template->getSubject(),
            ENT_QUOTES
        );
        $this->message = $this->emailMessageInterfaceFactory->create($this->messageData);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->productCollObj = $objectManager->create('\Magento\Catalog\Model\Product');
        $storeId = $template->getEmailStoreId();
        /** @var \Emarsys\Emarsys\Helper\Data $dataHelper */
        $dataHelper = $objectManager->create('\Emarsys\Emarsys\Helper\Data');
        $scopeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        $request = $objectManager->get('\Magento\Framework\App\Request\Http');

        list($magentoEventID, $configPath) = $dataHelper->getMagentoEventIdAndPath(
            $this->templateIdentifier,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        if (!$magentoEventID) {
            $this->message->setEmarsysData([
                    "emarsysPlaceholders" => '',
                    "emarsysEventId" => '',
                    "store_id" => $storeId
                ]);
            return $this;
        }
        $enableOptin = $scopeConfig->getValue('opt_in/optin_enable/enable_optin', 'websites', $this->storeManager->getStore($storeId)->getWebsiteId());
        if ($enableOptin) {
            $handle = $request->getFullActionName();
        }
        $emarsysEventMappingID = $dataHelper->getEmarsysEventMappingId($magentoEventID, $storeId);
        if (!$emarsysEventMappingID) {
            $this->message->setEmarsysData([
                    "emarsysPlaceholders" => '',
                    "emarsysEventId" => '',
                    "store_id" => $storeId
                ]);
            return $this;
        }

        $emarsysEventApiID = $dataHelper->getEmarsysEventApiId($magentoEventID, $storeId);
        if (!$emarsysEventApiID) {
            $this->message->setEmarsysData([
                    "emarsysPlaceholders" => '',
                    "emarsysEventId" => '',
                    "store_id" => $storeId
                ]);
            return $this;
        }

        $emarsysPlaceholders = $dataHelper->getPlaceHolders($emarsysEventMappingID);
        if (!$emarsysPlaceholders) {
            $emarsysPlaceholders = $dataHelper->insertFirstimeMappingPlaceholders($emarsysEventMappingID, $storeId);
            $emarsysPlaceholders = $dataHelper->getPlaceHolders($emarsysEventMappingID);
        }

        $emarsysHeaderPlaceholders = $dataHelper->emarsysHeaderPlaceholders($emarsysEventMappingID, $storeId);
        if (!$emarsysHeaderPlaceholders) {
            $emarsysInsertFirstHeaderPlaceholders = $dataHelper->insertFirstimeHeaderMappingPlaceholders(
                $emarsysEventMappingID,
                $storeId
            );
            $emarsysHeaderPlaceholders = $dataHelper->emarsysHeaderPlaceholders($emarsysEventMappingID, $storeId);
        }

        $emarsysFooterPlaceholders = $dataHelper->emarsysFooterPlaceholders($emarsysEventMappingID, $storeId);
        if (!$emarsysFooterPlaceholders) {
            $emarsysInsertFirstFooterPlaceholders = $dataHelper->insertFirstimeFooterMappingPlaceholders(
                $emarsysEventMappingID,
                $storeId
            );
            $emarsysFooterPlaceholders = $dataHelper->emarsysFooterPlaceholders($emarsysEventMappingID, $storeId);
        }

        $processedVariables = [];
        if ($order = $template->checkOrder()) {
            foreach ($order->getAllVisibleItems() as $item) {
                $orderData[] = $this->getOrderData($item);
            }
            $processedVariables['product_purchases'] = $orderData;
        }

        foreach ($emarsysHeaderPlaceholders as $key => $value) {
            if ($key == 'css_file_css_email_css') {
                continue;
            } else {
                $processedVariables['global'][$key] = $template->getProcessedVariable($value);
            }
        }
        foreach ($emarsysPlaceholders as $key => $value) {
            $processedVariables['global'][$key] = $template->getProcessedVariable($value);
        }
        foreach ($emarsysFooterPlaceholders as $key => $value) {
            $processedVariables['global'][$key] = $template->getProcessedVariable($value);
        }
        $this->message->setEmarsysData([
                "emarsysPlaceholders" => $processedVariables,
                "emarsysEventId" => $emarsysEventApiID,
                "store_id" => $storeId
            ]);

        return $this;
    }

    /**
     * @param $value
     * @return string
     */
    protected function _formatPrice($value = 0)
    {
        return sprintf('%01.2f', $value);
    }

    /**
     * @param $value
     * @return string
     */
    protected function _formatQty($value = 0)
    {
        return sprintf('%01.0f', $value);
    }

    /**
     * @param $item
     * @return array
     */
    public function getOrderData($item)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        try {
            $optionGlue = " - ";
            $optionSeparator = " : ";

            $unitTaxAmount = $item->getTaxAmount() / $item->getQtyOrdered();
            $order = [
                'unitary_price_exc_tax' => $this->_formatPrice($item->getPriceInclTax() - $unitTaxAmount),
                'unitary_price_inc_tax' => $this->_formatPrice($item->getPriceInclTax()),
                'unitary_tax_amount' => $this->_formatPrice($unitTaxAmount),
                'line_total_price_exc_tax' => $this->_formatPrice($item->getRowTotalInclTax() - $item->getTaxAmount()),
                'line_total_price_inc_tax' => $this->_formatPrice($item->getRowTotalInclTax()),
                'line_total_tax_amount' => $this->_formatPrice($item->getTaxAmount())
            ];
            $order['product_id'] = $item->getData('product_id');
            $order['product_type'] = $item->getData('product_type');
            $order['base_original_price'] = $this->_formatPrice($item->getData('base_original_price'));
            $order['sku'] = $item->getData('sku');
            $order['product_name'] = $item->getData('name');
            $order['product_weight'] = $item->getData('weight');
            $order['qty_ordered'] = $this->_formatQty($item->getData('qty_ordered'));
            $order['original_price'] = $this->_formatPrice($item->getData('original_price'));
            $order['price'] = $this->_formatPrice($item->getData('price'));
            $order['base_price'] = $this->_formatPrice($item->getData('base_price'));
            $order['tax_percent'] = $this->_formatPrice($item->getData('tax_percent'));
            $order['tax_amount'] = $this->_formatPrice($item->getData('tax_amount'));
            $order['discount_amount'] = $this->_formatPrice($item->getData('discount_amount'));
            $order['price_line_total'] = $this->_formatPrice($order['qty_ordered'] * $order['price']);

            $_product = $this->productCollObj->load($order['product_id']);

            $base_url = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore($item->getData('store_id'))
                ->getBaseUrl();
            $base_url = trim($base_url, '/');
            $order['_external_image_url'] = $base_url . '/media/catalog/product' . $_product->getData('thumbnail');
            $order['_url'] = $base_url . "/" . $_product->getUrlPath();
            $order['_url_name'] = $order['product_name'];
            $order['product_description'] = $_product->getData('description');
            $order['short_description'] = $_product->getData('short_description');
            $attributes = $_product->getAttributes();
            $prodData = $_product->getData();
            foreach ($attributes as $attribute) {
                if ($attribute->getFrontendInput() != "gallery") {
                    if (!isset($prodData[$attribute->getAttributeCode()])) {
                        //do nothing
                    } else {
                        $order['attribute_' . $attribute->getAttributeCode()] = $prodData[$attribute->getAttributeCode()];
                    }
                }
            }
            $order['full_options'] = [];
            $prodOptions = $item->getProductOptions();

            if (isset($prodOptions['attributes_info'])) {
                foreach ($prodOptions['attributes_info'] as $option) {
                    $order['full_options'][] = $option['label'] . $optionSeparator . $option['value'];
                }
                $order['full_options'] = implode($optionGlue, $order['full_options']);
            }

            $order = array_filter($order);
            $order['additional_data'] = ($item->getData('additional_data') ? $item->getData('additional_data') : "");

            return $order;
        } catch (\Exception $e) {
            $emarsysLogs = $objectManager->create('\Emarsys\Emarsys\Model\Logs');
            $emarsysLogs->addErrorLog(
                $e->getMessage(),
                $this->storeManager->getStore()->getId(),
                'TransportBuilder::getOrderData()'
            );
        }
    }

    /**
     * Add cc address
     *
     * @param array|string $address
     * @param string $name
     *
     * @return \Magento\Framework\Mail\Template\TransportBuilder
     */
    public function addCc($address, $name = '')
    {
        $this->addAddressByType('cc', $address, $name);

        return $this;
    }

    /**
     * Add to address
     *
     * @param array|string $address
     * @param string $name
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function addTo($address, $name = '')
    {
        $this->addAddressByType('to', $address, $name);

        return $this;
    }

    /**
     * Add bcc address
     *
     * @param array|string $address
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function addBcc($address)
    {
        $this->addAddressByType('bcc', $address);

        return $this;
    }

    /**
     * Set Reply-To Header
     *
     * @param string $email
     * @param string|null $name
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setReplyTo($email, $name = null)
    {
        $this->addAddressByType('replyTo', $email, $name);

        return $this;
    }

    /**
     * Set mail from address
     *
     * @param string|array $from
     *
     * @return $this
     * @throws InvalidArgumentException
     * @see setFromByScope()
     *
     * @deprecated 102.0.1 This function sets the from address but does not provide
     * a way of setting the correct from addresses based on the scope.
     */
    public function setFrom($from)
    {
        return $this->setFromByScope($from);
    }

    /**
     * Set mail from address by scopeId
     *
     * @param string|array $from
     * @param string|int $scopeId
     *
     * @return $this
     * @throws InvalidArgumentException
     * @throws MailException
     * @since 102.0.1
     */
    public function setFromByScope($from, $scopeId = null)
    {
        $result = $this->_senderResolver->resolve($from, $scopeId);
        $this->addAddressByType('from', $result['email'], $result['name']);

        return $this;
    }

    /**
     * Handles possible incoming types of email (string or array)
     *
     * @param string $addressType
     * @param string|array $email
     * @param string|null $name
     *
     * @return void
     * @throws InvalidArgumentException
     */
    private function addAddressByType(string $addressType, $email, ?string $name = null): void
    {
        if (is_string($email)) {
            $this->messageData[$addressType][] = $this->addressConverter->convert($email, $name);
            return;
        }

        $this->messageData[$addressType] = $this->messageData[$addressType] ?? [];
        $convertedAddressArray = $this->addressConverter->convertMany($email);

        $this->messageData[$addressType] = array_merge(
            $this->messageData[$addressType],
            $convertedAddressArray
        );
    }

    /**
     * Get mail transport
     *
     * @return TransportInterface
     * @throws LocalizedException
     */
    public function getTransport()
    {
        try {
            $this->prepareMessage();
            $mailTransport = $this->mailTransportFactory->create(['message' => clone $this->message]);
        } finally {
            $this->reset();
        }

        return $mailTransport;
    }

    /**
     * @return $this|\Magento\Framework\Mail\Template\TransportBuilder
     */
    protected function reset()
    {
        $this->messageData = [];
        $this->templateIdentifier = null;
        $this->templateVars = null;
        $this->templateOptions = null;
        return $this;
    }
}
