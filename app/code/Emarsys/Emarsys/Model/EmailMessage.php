<?php

namespace Emarsys\Emarsys\Model;

/**
 * Class EmailMessage
 * @package Emarsys\Emarsys\Model
 */
class EmailMessage extends \Magento\Framework\Mail\EmailMessage
{
    /**
     * @var array
     */
    protected $emarsysData = [];

    /**
     * @param $emarsysData
     * @return mixed
     */
    public function setEmarsysData($emarsysData)
    {
        return $this->emarsysData = $emarsysData;
    }

    /**
     * @return array
     */
    public function getEmarsysData()
    {
        return $this->emarsysData;
    }

    /**
     * @return bool|mixed
     * @throws \ReflectionException
     */
    public function getZendMessage()
    {
        $reflectionClass = new \ReflectionClass(\Magento\Framework\Mail\EmailMessage::class);
        if ($reflectionClass->hasProperty('message')) {
            $reflection = $reflectionClass->getProperty('message');
            $reflection->setAccessible(true);
            return $reflection->getValue($this);
        } else {
            return false;
        }
    }
}
