<?php
/**
 * @category   Emarsys
 * @package    Emarsys_Emarsys
 * @copyright  Copyright (c) 2017 Emarsys. (http://www.emarsys.net/)
 */
namespace Emarsys\Emarsys\Cron;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Filesystem\Io\File;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Emarsys\Emarsys\Helper\Data;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Translate\Inline\StateInterface;

/**
 * Class CleanLog
 * @package Emarsys\Emarsys\Cron
 */
class CleanLog
{
    /**
     * @var DeploymentConfig
     */
    protected $config;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var DirectoryList
     */
    protected $baseDirPath;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * @var Config
     */
    protected $resourceConfig;

    /**
     * @var File
     */
    protected $ioFile;

    /**
     * @var Data
     */
    protected $emarsysHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * CleanLog constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param DirectoryList $baseDirPath
     * @param ResourceConnection $resource
     * @param File $ioFile
     * @param Config $resourceConfig
     * @param DateTime $date
     * @param Data $emarsysHelper
     * @param DeploymentConfig $config
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     * @param StateInterface $inlineTranslation
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        DirectoryList $baseDirPath,
        ResourceConnection $resource,
        File $ioFile,
        Config $resourceConfig,
        DateTime $date,
        Data $emarsysHelper,
        DeploymentConfig $config,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        StateInterface $inlineTranslation
    ) {
        $this->config = $config;
        $this->date = $date;
        $this->baseDirPath = $baseDirPath;
        $this->scopeConfig = $scopeConfig;
        $this->_resource = $resource;
        $this->resourceConfig = $resourceConfig;
        $this->ioFile = $ioFile;
        $this->emarsysHelper = $emarsysHelper;
        $this->storeManager = $storeManager;
        $this->_logger = $logger;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
    }

    /**
     * Executes log cleaning
     *
     * @return void
     * @throws LocalizedException
     * @throws MailException
     * @throws \Exception
     */
    public function execute()
    {
        $store = $this->storeManager->getStore();
        $websiteId = $store->getWebsiteId();
        $storeId = $store->getStoreId();
        $scopeType = 'websites';

        $configData = $this->config->getConfigData();
        $connection = $configData['db']['connection']['default'];

        $username = $connection['username'];
        $password = $connection['password'];
        $hostname = $connection['host'];
        $database = $connection['dbname'];

        $username = escapeshellcmd($username);
        $password = escapeshellcmd($password);
        $hostname = escapeshellcmd($hostname);
        $database = escapeshellcmd($database);

        $logCleaning = $this->scopeConfig->getValue('logs/log_setting/log_cleaning', $scopeType, $websiteId);
        if ($logCleaning == '' && $websiteId == 1) {
            $logCleaning = $this->scopeConfig->getValue('logs/log_setting/log_cleaning');
        }

        $archive = $this->scopeConfig->getValue('logs/log_setting/archive_data', $scopeType, $websiteId);
        if ($archive == '' && $websiteId == 1) {
            $archive = $this->scopeConfig->getValue('logs/log_setting/archive_data');
        }

        if ($logCleaning) {
            $logCleaningDays = $this->scopeConfig->getValue('logs/log_setting/log_days', $scopeType, $websiteId);
            if ($logCleaningDays == '' && $websiteId == 1) {
                $logCleaningDays = $this->scopeConfig->getValue('logs/log_setting/log_days');
            }
            $cleanUpDate = $this->emarsysHelper->getDateTimeWithOffset((int) $logCleaningDays);
            /* Create archive folder*/

            $varDir = $this->baseDirPath->getRoot() . "/";
            $archivePath = $this->scopeConfig->getValue('logs/log_setting/archive_datapath', $scopeType, $websiteId);
            if ($archivePath == '' && $storeId == 1) {
                $archivePath = $this->scopeConfig->getValue('logs/log_setting/archive_datapath');
            }

            $archivePath = rtrim($archivePath, '/') . '/';
            $archiveFolder = $varDir . $archivePath . $this->date->date('Y-m-d');
            $this->ioFile->checkAndCreateFolder($archiveFolder);

            /* backup sql file*/
            $backupFile = $archiveFolder . "/emarsys_logs.sql";

            /* logs table */
            $logTables = array_map(function (string $tableName) {
                return $this->resourceConfig->getTable($tableName);
            }, ['emarsys_log_details', 'emarsys_log_cron_schedule']);

            /* Dump record of logs table in one file */
            $successLog = 0;
            $errorLog = 0;

            try {
                if ($archive == 'archive') {
                    $logTable = implode(' ', $logTables);
                    $command = "mysqldump -u$username -p$password -h$hostname $database $logTable > $backupFile";
                    system(escapeshellarg($command), $result);
                }

                /* Delete record from log_details tables */
                $sqlConnection = $this->_resource->getConnection(ResourceConnection::DEFAULT_CONNECTION);
                foreach ($logTables as $table) {
                    $sqlConnection->delete($table, $sqlConnection->quoteInto('created_at <= ?', $cleanUpDate));
                }
                $successLog = 1;
            } catch (\Exception $e) {
                $errorLog = 1;
                $errorResult[] = $e->getMessage();
            }

            /**
             * Email send if failure
             */

            $errorEmailData = $this->emarsysHelper->logErrorSenderEmail();
            $senderEmailId = $errorEmailData['email'];
            $senderEmailName = $errorEmailData['name'];
            $logEmailRecipient = $this->scopeConfig->getValue('logs/log_setting/log_email_recipient', $scopeType, $websiteId);
            if ($logEmailRecipient == '' && $websiteId == 1) {
                $logEmailRecipient = $this->scopeConfig->getValue('logs/log_setting/log_email_recipient');
            }
            $to = explode(',', $logEmailRecipient);

            if ($successLog == 1) {
                $templateOptions = ['area' => Area::AREA_FRONTEND, 'store' => $storeId];
                $templateVars = [
                    'store' => $storeId,
                    'message' => $backupFile
                ];
                $from = [
                    'email' => $senderEmailId,
                    'name' => $senderEmailName
                ];
                $this->inlineTranslation->suspend();
                $transport = $this->_transportBuilder->setTemplateIdentifier('emarsys_log_cleaning_template')
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($to)
                    ->getTransport();
                $transport->sendMessage();
                $this->inlineTranslation->resume();
            }

            /*Send email for Error in archived File*/
            if ($errorLog == 1) {
                $errorMessage = implode(",", $errorResult);
                $templateOptions = ['area' => Area::AREA_FRONTEND, 'store' => $storeId];
                $templateVars = [
                    'store' => $storeId,
                    'message' => $errorMessage
                ];
                $from = [
                    'email' => $senderEmailId,
                    'name' => $senderEmailName
                ];
                $this->inlineTranslation->suspend();
                $transport = $this->_transportBuilder->setTemplateIdentifier('emarsys_log_cleaning_error_template')
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($to)
                    ->getTransport();
                $transport->sendMessage();
                $this->inlineTranslation->resume();
            }

            /**
             * Delete the archived Data older than $deleteArchivedData
             */

            if ($archive == 'archive') {
                $deleteArchivedData = $this->scopeConfig->getValue('logs/log_setting/delete_archive_days', $scopeType, $websiteId); // Number of days to delete old archived data
                if ($deleteArchivedData == '' && $websiteId == 1) {
                    $deleteArchivedData = $this->scopeConfig->getValue('logs/log_setting/delete_archive_days');
                }
                $deleteDate = $this->date->date('Y-m-d', strtotime("-" . $deleteArchivedData . " days"));
                $archiveFolderPath = $varDir . $archivePath;
                $dir = new \DirectoryIterator($archiveFolderPath);                      //Sub-dir inside archive dir
                foreach ($dir as $fileinfo) {
                    if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                        $path = rtrim($archiveFolderPath, '/') . "/" . $fileinfo->getFilename();        //get sub-dir name
                        $stat = stat($path);
                        $folderCreateDate = $this->date->date('Y-m-d', $stat['ctime']);
                        /* If create date of sub-dir less than delete date of sub-dir then delete sub-dir */
                        if ($folderCreateDate <= $deleteDate && is_dir($path)) {
                            array_map('unlink', glob($path . "/*"));                      //delete all files inside directory
                            rmdir($path);                                               //delete directory
                        }
                    }
                }
            }
        }
    }
}
