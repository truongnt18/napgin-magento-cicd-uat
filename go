#!/usr/bin/env bash

set -e
set -o pipefail

APP_NAME="napgin"
MDC_PHP_IMAGE="magento/magento-cloud-docker-php:7.4-cli-1.2"
MDC_PHP_COMPOSER="composer:1.9.3"
MDC_RABBIT_MQ_IMAGE="rabbitmq:3.8.7"
MDC_ELASTICSEARCH_IMAGE="magento/magento-cloud-docker-elasticsearch:7.6-1.2"
MDC_MYSQL_IMAGE="mysql:5.6"
PHPUNIT_IMAGE="phpunit/phpunit:5.3.4"
TERRAFORM_DOCKER_IMAGE="hashicorp/terraform:0.12.8"
BUCKET="artifact-napgin-sit"
SONARQUBE_CLI_IMAGE="sonarsource/sonar-scanner-cli:4.5"


TASK=$1
ARGS=${@:2}

assume_role() {
  account_id="$1"
  role="$2"

  credentials=$(aws sts assume-role --role-arn "arn:aws:iam::${account_id}:role/${role}" \
                                      --role-session-name initial --duration-seconds 2700 | jq '.Credentials')
  export AWS_ACCESS_KEY_ID=$(echo "${credentials}" | jq -r .AccessKeyId)
  export AWS_SECRET_ACCESS_KEY=$(echo "${credentials}" | jq -r .SecretAccessKey)
  export AWS_SESSION_TOKEN=$(echo "${credentials}" | jq -r .SessionToken)
  unset AWS_SECURITY_TOKEN
}

runs_inside_gocd() {
  test -n "${GO_JOB_NAME}"
}

docker_ensure_volume() {
  docker volume inspect $1 >/dev/null 2>&1 || docker volume create $1 >/dev/null 2>&1
}

docker_ensure_network() {
  docker network inspect $1 >/dev/null 2>&1 || docker network create $1 >/dev/null 2>&1
}

docker_run() {
  if [ -z "${DOCKER_IMAGE}" ]; then
    echo -n "Building toolchain container; this might take a while..."
    DOCKER_IMAGE=$(docker build ${DOCKER_BUILD_ARGS} . -q)
    echo " Done."
  fi

  if runs_inside_gocd; then
    DOCKER_ARGS="${DOCKER_ARGS} -v godata:/godata -w $(pwd)"
  else
    DOCKER_ARGS="${DOCKER_ARGS} -it -v $(pwd):/workspace:cached -w /workspace"
  fi

  DOCKER_ARGS="${DOCKER_ARGS} -v ${HOME}/.aws:/root/.aws"
  
  docker_ensure_network magento_network
  docker run --rm  \
    --net magento_network \
    --hostname $(hostname) \
    --env-file <(env | grep AWS_) \
    --env-file <(env | grep TF_) \
    ${DOCKER_ARGS} ${DOCKER_IMAGE} "$@"
}

gradle() {
  docker_ensure_volume enotification-gradle-cache
  docker_ensure_volume sonarqube-cache

  DOCKER_IMAGE="${JAVA_DOCKER_IMAGE}"
  DOCKER_ARGS="${DOCKER_ARGS} -v enotification-gradle-cache:/root/.gradle"
  DOCKER_ARGS="${DOCKER_ARGS} -v sonarqube-cache:/root/.sonar/cache"

  docker_run ./gradlew $@
}

mdc_php_root() {
  docker_ensure_volume mdc-space
  docker_ensure_volume mdc-composer-cache
  docker_ensure_network magento_network
  DOCKER_IMAGE="${MDC_PHP_IMAGE}"
  DOCKER_ARGS="${DOCKER_ARGS} -v mdc-composer-cache:/root/.composer -v mdc-space:/usr -v $PWD:/app"

  docker_run $@
}

mdc_php() {
  if runs_inside_gocd; then
    local docker_user_args="-u $(id -u):$(id -g)"
  else
    local docker_user_args="-u $(id -u):$(id -g)"
  fi

  DOCKER_IMAGE="${MDC_PHP_IMAGE}"
  DOCKER_ARGS="${DOCKER_ARGS}  "

  docker_run $@
}


phpunit() {
  DOCKER_IMAGE="${PHPUNIT_IMAGE}"
  docker_run $@
}


mdc_php_composer(){
  docker_ensure_volume mdc-space
  docker_ensure_volume mdc-composer-cache
  docker_ensure_network magento_network
  docker run --rm  \
    --net magento_network \
    --volume $PWD:/app \
    --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp \
    --volume mdc-composer-cache:/root/.composer \
    --volume mdc-space:/usr \
    ${MDC_PHP_IMAGE} $@
}

mdc_composer() {
  if runs_inside_gocd; then
    local docker_user_args="-u $(id -u):$(id -g)"
  else
    local docker_user_args="-u $(id -u):$(id -g)"
  fi
 docker_ensure_network magento_network 
  docker run --rm \
  ${docker_user_args} \
  --net magento_network \
  --volume $PWD:/app \
  --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp \
  ${MDC_PHP_COMPOSER} $@
}

# mdc_php() {
#   docker_ensure_volume mdc-space
#   docker_ensure_volume mdc-composer-cache
#   DOCKER_IMAGE="${MDC_PHP_IMAGE}"
#   DOCKER_ARGS="${DOCKER_ARGS} -v mdc-space:/usr"
#   DOCKER_ARGS="${DOCKER_ARGS} -v mdc-composer-cache:/.composer"
#   DOCKER_ARGS="${DOCKER_ARGS} -v $(pwd):/var/lib/go-agent/pipelines/eordering-mdc-integrationtest/mdc-source"
#   docker_run $@
# }

mdc_phpunit_integration_test() {
    docker_ensure_volume mdc-space
    docker_ensure_volume mdc-composer-cache
    docker_ensure_network magento_network
    docker run --rm \
        --net magento_network \
        --volume $PWD:/app \
        --volume ${COMPOSER_HOME:-$HOME/.composer}:/tmp \
        --volume mdc-composer-cache:/.composer \
        --volume mdc-space:/usr \
        -w /app/dev/tests/integration/ \
        ${MDC_PHP_IMAGE} $@
}

sonarqube_scanner() {
    local docker_user_args="-u $(id -u):$(id -g)"

    docker run --rm \
        ${docker_user_args} \
        --volume $PWD:/usr/src \
        ${SONARQUBE_CLI_IMAGE} $@
}


tf() {
  if runs_inside_gocd; then
    local docker_user_args="-u $(id -u)"
  else
    local docker_user_args="-u $(id -u)"
  fi

  DOCKER_IMAGE="${TERRAFORM_DOCKER_IMAGE}"
  DOCKER_ARGS="${DOCKER_ARGS} ${docker_user_args}"

  docker_run "$@"
}

add_container_tag() {
  local repository_name=$1
  local image_tag=$2
  local new_image_tag=$3

  (
    assume_role $(account_id_for_name "tools") "push-containers"

    local image_manifest=$(aws ecr batch-get-image --region ap-southeast-1 \
      --repository-name ${repository_name} \
      --image-ids imageTag=${image_tag} \
      --query 'images[].imageManifest' \
      --output text)

    aws ecr put-image --region ap-southeast-1 \
      --repository-name ${repository_name} \
      --image-tag "${new_image_tag}" \
      --image-manifest "${image_manifest}"
  )
}

fetch_secret() {
  (
    account_name="$1"
    secret_name="$2"
    assume_role $(account_id_for_name ${account_name}) "deploy-app"
    echo `aws secretsmanager get-secret-value --secret-id ${secret_name} --query SecretString --output text --region ap-southeast-1`
  )
}

build_fetch_secret() {
  (
    account_name="$1"
    role_name="$2"
    secret_name="$3"
    assume_role "${account_name}" "${role_name}"
    echo $(aws secretsmanager get-secret-value --secret-id ${secret_name} --query SecretString --output text --region ap-southeast-1)
  )
}

getAuthJson() {
  echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "composer_auth")  > auth.json

  # For debug only, will remove once testing done
  cat auth.json
}

# help__pushMagentoArtifactToNexus="Push Magento artifact to nexus"
# task_pushMagentoArtifactToNexus() {
#   local env=$1

#   if [ -z "${env}" ] ; then
#     echo "Needs environment"
#     exit 1
#   fi
#   ls -lah
#   UAT_NEXUS_SECRETS=$(echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "mdc-build/nexus_auth"))
#   NEXUS_KEY=$(echo "${UAT_NEXUS_SECRETS}" | jq -r .NEXUS_PUSH_AUTH)
#   # Push MagentoCI.zip to nexus
#   curl -H "Authorization:Basic ${NEXUS_KEY}" --upload-file MagentoCI.zip https://nexus.central.tech/repository/e-ordering-mdc/artifact-eordering-${env}/GoCD/${GO_PIPELINE_COUNTER}/MagentoCI.zip
# }


# help__pullMagentoArtifactFromNexus="Pull Magento artifact from nexus"
# task_pullMagentoArtifactFromNexus() {
#   local env=$1

#   if [ -z "${env}" ] ; then
#     echo "Needs environment"
#     exit 1
#   fi
#   UAT_NEXUS_SECRETS=$(echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "mdc-build/nexus_auth"))
#   local NEXUS_KEY=$(echo "${UAT_NEXUS_SECRETS}" | jq -r .NEXUS_PUSH_AUTH)
#   curl -H "Authorization:Basic ${NEXUS_KEY}" https://nexus.central.tech/repository/e-ordering-mdc/artifact-eordering-${env}/GoCD/1/MagentoCI.zip -o MagentoCI.zip
# }


fixDockerVolumeMountPermision(){
  echo "--- FIX DOCKER FILES PERMISSION START ---"
  # WORKAROUND: fix docker file permission
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  echo "--- FIX DOCKER FILES PERMISSION STOP ---"
}

unitTestTrap() {
  echo "--- UNIT TEST TRAP START ---"
  fixDockerVolumeMountPermision
  echo "--- UNIT TEST TRAP END ---"
}


integrationTestTrap() {
  echo "--- INTEGRATION TEST TRAP START ---"
  fixDockerVolumeMountPermision
  task_stopIntegrationDb
  task_stopIntegrationElasticsearch
  task_stopIntegrationRabbitmq
  echo "--- INTEGRATION TEST TRAP END ---"
}


help__fixDockerVolumeFilesPermission="fix docker file generated's permission"
task_fixDockerVolumeFilesPermission() {
    fixDockerVolumeMountPermision
}


help__getAuthJsonFile="Get Magento AuthJson File from Secrets Manager"
task_getAuthJsonFile() {
    getAuthJson
}

help__startIntegrationDb="Start the integration database locally and bind port to port 3306"
task_startIntegrationDb() {
  task_stopIntegrationDb

  mkdir -p data_mysql

  if runs_inside_gocd; then
    port_forward=""
  else
    port_forward="-p 3306:3306"
  fi

  docker_ensure_network magento_network

  docker run --rm --name dbintegration $port_forward \
    --net magento_network \
    -e MYSQL_ROOT_HOST=% \
    -e MYSQL_ROOT_PASSWORD=magento \
    -e MYSQL_DATABASE=magento_integration_test \
    -d $MDC_MYSQL_IMAGE
}

help__stopIntegrationDb="Stop integrationdb"
task_stopIntegrationDb() {
  if docker ps | grep "dbintegration" > /dev/null; then
    docker stop dbintegration
  fi
}

help__startIntegrationElasticsearch="Start the integration elasticsearch locally"
task_startIntegrationElasticsearch() {
  task_stopIntegrationElasticsearch

  mkdir -p data_elasticsearch

  if runs_inside_gocd; then
    port_forward=""
  else
    port_forward="-p 9200:9200 -p 9300:9300"
  fi

  docker run --rm --name elasticsearch $port_forward \
    --net magento_network \
    -e ES_JAVA_OPTS='-Xms512m -Xmx512m' \
    -d $MDC_ELASTICSEARCH_IMAGE
}

help__stopIntegrationElasticsearch="Stop integration elasticsearch"
task_stopIntegrationElasticsearch() {
  if docker ps | grep "elasticsearch" > /dev/null; then
    docker stop elasticsearch
  fi
}

help__startIntegrationRabbitmq="Start the integration Rabbitmq locally"
task_startIntegrationRabbitmq() {
  task_stopIntegrationRabbitmq

  mkdir -p data_rabbitmq

  if runs_inside_gocd; then
    port_forward=""
  else
    port_forward="-p 5672:5672 -p 15672:15672"
  fi

  docker run --rm --name rabbitmq $port_forward \
    --net magento_network \
    -d $MDC_RABBIT_MQ_IMAGE
}

help__stopIntegrationRabbitmq="Stop integration Rabbitmq"
task_stopIntegrationRabbitmq() {
  if docker ps | grep "rabbitmq" > /dev/null; then
    docker stop rabbitmq
  fi
}


help__mdcSonarqube="MDC Sonarqube Code Scan"
task_mdcSonarqube() {
    trap unitTestTrap exit err
  
  #getAuthJson
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  mdc_php_root useradd -s /bin/bash -d /app -m -G sudo $(id -u)
  mdc_php_root apt-get update
  mdc_php_root apt-get install -y openssh-client autoconf zlib1g-dev
  mdc_php_root pecl shell-test grpc && echo 'Package grpc installed' || (pecl install grpc )
  mdc_php_root 'touch /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_root 'echo "extension=grpc.so" > /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_composer curl https://getcomposer.org/download/1.10.13/composer.phar --output composer.phar
  mdc_php_composer ls -lah
  mdc_php_composer mv composer.phar /usr/local/bin/composer
  mdc_php_composer chmod +x /usr/local/bin/composer
  mdc_php_composer php -v
  mdc_php_composer ls -lah
  mdc_php_composer composer -V
  mdc_php_composer composer install --prefer-dist
  mdc_php_composer bin/magento setup:di:compile
  sonarqube_scanner sonar-scanner -Dproject.settings=sonar-scanner.properties
}


help__mdcUnitTest="MDC Unit tests"
task_mdcUnitTest() {
  trap unitTestTrap exit err
  
  #getAuthJson
 
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  mdc_php_root useradd -s /bin/bash -d /app -m -G sudo $(id -u)
  mdc_php_root apt-get update
  mdc_php_root apt-get install -y openssh-client autoconf zlib1g-dev
  mdc_php_root pecl shell-test grpc && echo 'Package grpc installed' || (pecl install grpc )
  mdc_php_root 'touch /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_root 'echo "extension=grpc.so" > /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_composer curl https://getcomposer.org/download/1.10.13/composer.phar --output composer.phar
  mdc_php_composer ls -lah
  mdc_php_composer mv composer.phar /usr/local/bin/composer
  mdc_php_composer chmod +x /usr/local/bin/composer
  mdc_php_composer php -v
  mdc_php_composer ls -lah
  mdc_php_composer composer -V
  mdc_php_composer composer install
  mdc_php_composer bin/magento setup:di:compile
  mdc_php_composer pwd
  mdc_php_composer vendor/bin/phpunit
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  mdc_php_composer ls -lah
}

help__mdcIntegrationTest="MDC Integration tests"
task_mdcIntegrationTest() {
  # COMPOSER_AUTH=`cat /home/truong/Downloads/auth.json`
  # echo $COMPOSER_AUTH > auth.json

  # Trap it!
  trap integrationTestTrap exit err

  # trap task_stopIntegrationDb EXIT
  # trap task_stopIntegrationElasticsearch EXIT
  # trap task_stopIntegrationRabbitmq EXIT

  # Start local services MySQL, Elasticsearch, Rabbitmq
  task_startIntegrationElasticsearch
  task_startIntegrationRabbitmq
  task_startIntegrationDb
  
  # getAuthJson
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  mdc_php_root apt-get update
  mdc_php_root apt-get install -y openssh-client autoconf zlib1g-dev
  mdc_php_root pecl shell-test grpc && echo 'Package grpc installed' || (pecl install grpc )
  mdc_php_root 'touch /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_root 'echo "extension=grpc.so" > /usr/local/etc/php/conf.d/grpc.ini'
  mdc_php_composer curl https://getcomposer.org/download/1.10.13/composer.phar --output composer.phar
  mdc_php_composer ls -lah
  mdc_php_composer mv composer.phar /usr/local/bin/composer
  mdc_php_composer chmod +x /usr/local/bin/composer
  mdc_php_composer php -v
  mdc_php_composer ls -lah
  mdc_php_composer composer -V
  mdc_php_composer composer install
  mdc_php_composer 'php integration/read-config.php'
  mdc_php_composer 'bin/magento setup:di:compile'
  
  mdc_php_composer "cp integration/phpunit.xml dev/tests/integration/phpunit.xml"
  mdc_php_composer "cp integration/install-config-mysql.php dev/tests/integration/etc/install-config-mysql.php"
  mdc_php_composer "cp integration/config-global.php dev/tests/integration/etc/config-global.php"
  
  mdc_phpunit_integration_test pwd
  mdc_phpunit_integration_test ../../../vendor/bin/phpunit
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  mdc_php_composer ls -lah

  # Stopped all docker services
  task_stopIntegrationDb
  task_stopIntegrationElasticsearch
  task_stopIntegrationRabbitmq
}

getAuthShareJson() {
  echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "composer_auth")  > auth_shared.json
  # For debug only, will remove once testing done
  cat auth_shared.json
}

help__mdcDeployment="MDC Deployment"
task_mdcDeployment() {
  trap unitTestTrap exit err
  # echo $(build_fetch_secret "491229787580" "GoCDAssumedIamRole" "mdc-build/auth_json")  > auth.json
  # getAuthJson
  mdc_php_composer chown -R $(id -u):$(id -g) * .[^.]*
  pwd
  ls -lah
  assume_role "178408455695" "GoCDAssumedIamRole"
  aws s3 ls

  mdc_php_composer curl https://getcomposer.org/download/1.10.13/composer.phar --output composer.phar
  mdc_php_composer ls -lah
  mdc_php_composer mv composer.phar /usr/local/bin/composer
  mdc_php_composer chmod +x /usr/local/bin/composer
  mdc_php_composer php -v
  mdc_php_composer ls -lah
  mdc_php_composer composer -V
  mdc_php_composer composer install --prefer-dist
  mdc_php_composer 'php integration/read-config.php'
  mdc_php_composer 'bin/magento setup:di:compile'
  mdc_php_composer php -d memory_limit=-1 bin/magento setup:static-content:deploy -f en_US th_TH
  mdc_php_composer rm -rf var  pub/media app/etc/env.php

  zip -r MagentoCI.zip .
  aws s3 cp MagentoCI.zip s3://${BUCKET}/GoCD/${GO_PIPELINE_COUNTER}/MagentoCI.zip --acl public-read
}

help__mdcPushArtifact="MDC Ansible"
task_mdcPushArtifact() {
  assume_role "178408455695" "GoCDAssumedIamRole"
  zip -r MagentoCI.zip .
  aws s3 cp MagentoCI.zip s3://${BUCKET}/GoCD/${GO_PIPELINE_COUNTER}/MagentoCI.zip --acl public-read
}

help__mdcAnsible="MDC Ansible"
task_mdcAnsible() {
  # not run in docker, so skipped docker files cleanup trap
  # trap unitTestTrap exit err
  cp -r ../mdc-config/napgin/mdc/uat/ansible/ .
  cd ./ansible
  echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "composer_auth")  > ./templates/auth_shared.json
  build_fetch_secret_pem "178408455695" "GoCDAssumedIamRole" "mdc-build/servers_key"
  UAT_SECRET=$(echo $(build_fetch_secret "178408455695" "GoCDAssumedIamRole" "napgin-ssh-key-nonprod"))
  # DATABASE_PASSWORD=$(echo "${UAT_SECRET}" | jq -r .DATABASE_PASSWORD)
  # OAUTH_CLIENT_SECRET=$(echo "${UAT_SECRET}" | jq -r .OAUTH_CLIENT_SECRET)
  chmod 400 servers_key.pem
  set -- $ARGS
  ansible-galaxy install ansistrano.deploy ansistrano.rollback
  ansible-playbook -i hosts.ini deploy.yml --private-key servers_key.pem --extra-var "ansistrano_get_url=https://${BUCKET}.s3-ap-southeast-1.amazonaws.com/GoCD/${GO_PIPELINE_COUNTER}/MagentoCI.zip ansistrano_deploy_from=../ oauth_client_id=${1} oauth_client_secret=${OAUTH_CLIENT_SECRET} servicebus_url=${2} oauth_server_url=${3} application_id=${4} database_user=${5} database_password=${DATABASE_PASSWORD} database_host=${6} database_name=${7} redis_url=${8} application_url=${9} elasticsearch_url=${10} elasticsearch_ip=${11} ansistrano_allow_anonymous_stats= no"
}
list_all_helps() {
  compgen -v | egrep "^help__.*"
}

NEW_LINE=$'\n'
if type -t "task_$TASK" &>/dev/null; then
  task_$TASK $ARGS
else
  echo "usage: $0 <task> [<..args>]"
  echo "task:"

  HELPS=""
  for help in $(list_all_helps)
  do

    HELPS="$HELPS    ${help/help__/} |-- ${!help}$NEW_LINE"
  done

  echo "$HELPS" | column -t -s "|"
  exit 1
fi