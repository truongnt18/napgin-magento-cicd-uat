<?php

try {
    require __DIR__ . '/../app/bootstrap.php';
} catch (\Exception $e) {
    echo 'Failure to require Magento bootstrap';
}

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$objManager = $bootstrap->getObjectManager();
/** @var \Magento\Framework\Module\DbVersionInfo $dbStatusValidator */
$dbStatusValidator = $objManager->get(\Magento\Framework\Module\DbVersionInfo::class);
try {
    $errors = $dbStatusValidator->getDbVersionErrors();
} catch (\Exception $e) {
    $errors = 1;
}
echo $errors ? 0 : 1;

